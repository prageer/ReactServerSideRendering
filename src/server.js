'use strict';

import path from 'path';
import {Server} from 'http';
import Express from 'express';
import React from 'react';
import {renderToString} from 'react-dom/server';
import {match, RouterContext} from 'react-router';
import routes from './routes';
import NotFoundPage from './components/NotFoundPage';
import dotenv from 'dotenv';
import mysql from 'mysql2';
import bodyParser from 'body-parser'; // PARSE HTML BODY

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';


// initialize the server and configure support for ejs templates
const app = new Express();
const server = new Server(app);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.json());
// define the folder that will be used for static assets
app.use(Express.static(path.join(__dirname, 'static')));
app.use(Express.static(path.join(__dirname, '../node_modules')));

dotenv.load();

const connection = mysql.createConnection({host: process.env.DBHOST,
  user: process.env.DBUSER, password: process.env.DBPASSWORD,
  database: process.env.DATABASE});

app.get('/db', function(request, response) {
  connection.query('SELECT * FROM `test_table`',
      function(err, results, fields) {
        if (err) {
          console.error(err); response.send('Error ' + err);
        } else {
          response.render('pages/db', {results: results} );
        }
      });
});

// universal routing and rendering
app.get('*', (req, res) => {  

  const store = createStore(reducers, applyMiddleware(thunk));

  match(
    {routes, location: req.url},
    (err, redirectLocation, renderProps) => {
      // in case of error display the error message
      if (err) {
        return res.status(500).send(err.message);
      }

      // in case of redirect propagate the redirect to the browser
      if (redirectLocation) {
        return res.redirect(302,
            redirectLocation.pathname + redirectLocation.search);
      }

      // generate the React markup for the current route
      let markup;
      if (renderProps) {


        // if the current route matched we have renderProps
        markup = renderToString(
          <Provider store={store}>
            <RouterContext {...renderProps} />
          </Provider>
        );
      } else {
        // otherwise we can render a 404 page
        markup = renderToString(<NotFoundPage />);
        res.status(404);
      }

      // render the index template with the embedded React markup
      return res.render('pages/index', {markup: markup});
    }
  );
});

app.post('/test', function(request, response) {

  if(request.body.contents === "") {
      return response.status(400).json({
          error: "EMPTY CONTENTS",
          code: 2
      });
  }

  // CREATE NEW MEMO
  let question = request.body.contents.question;
  let answer = request.body.contents.answer;
  let hint = request.body.contents.hint;

  let query = 'insert into `test_table_new` (question, answer, hint) values ("'+question+'","'+answer+'", "'+hint+'")';
  
  connection.query(query,
  function(err, results, fields) {
    if (err) {
       return response.status(400).json({
            error: "CONNECTION ERROR",
            code: 2
        });
    } else {
      return response.json({ success: true });  
    }
  });
  

});

// start the server
const port = process.env.PORT || 5000;
server.listen(port, (err) => {
  if (err) {
    return console.error(err);
  }
  console.info(`Server running on http://localhost:${port}`);
});
