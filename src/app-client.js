import React from 'react';
import ReactDOM from 'react-dom';
import AppRoutes from './components/AppRoutes';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';

const initialState = window.__REDUX_STATE__ 
const store = createStore(reducers, initialState, applyMiddleware(thunk));

window.onload = () => {
  ReactDOM.render(<Provider store={store}><AppRoutes/></Provider>, document.getElementById('main'));
};