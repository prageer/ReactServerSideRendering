/* eslint max-len: 0 */
'use strict';

import React from 'react';
import {Router, browserHistory} from 'react-router';
import routes from '../routes';

/** class app routes */
export default class AppRoutes extends React.Component {
  /** render function
   *  @return {JSX} */
  render() {
    return (
      <Router history={browserHistory} routes={routes} onUpdate={() => window.scrollTo(0, 0)} />
    );
  }
}
