'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { testPostRequest } from '../actions/test';


/** class index page */
class Test extends React.Component {
  /** render function
   *  @return {JSX} */

   constructor(props){
    super(props);    
   }
  
  handleSubmitEvent(event) {    
    event.preventDefault();

    var item = {
      question: this.refs.question.value.trim(),
      answer: this.refs.answer.value.trim(),
      hint: this.refs.hint.value
    };

    this.props.testPostRequest(item);    

  }

  render() {
    let styleRequired = {
      color: "#ffaaaa"
    };

    return (
      <form onSubmit={this.handleSubmitEvent.bind(this)}>
      <div className="container">
        <h3 className="page-header">Add New Item</h3>

        <div className="form-group">
          <label htmlFor="listItemName">Question <span style={styleRequired}>*</span></label>
          <input type="text" className="form-control" id="listItemName" placeholder="Enter Question" required ref="question" />
        </div>

        <div className="form-group">
          <label htmlFor="listItemDescription">Answer<span style={styleRequired}>*</span></label>
          <textarea className="form-control" rows="3" id="listItemDescription" placeholder="Enter Answer" required ref="answer"></textarea>
        </div>

        <div className="form-group">
          <label htmlFor="listItemName">Hint <span style={styleRequired}>*</span></label>
          <input type="text" className="form-control" id="listItemName" placeholder="Enter Hint" required ref="hint" />
        </div>

        <hr />

        <button type="submit" className="btn btn-primary">Add</button>
        
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        testStatus: state.test.post
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        testPostRequest: (contents) => {
            return dispatch(testPostRequest(contents));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Test);