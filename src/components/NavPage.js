/* eslint max-len: 0 */
'use strict';

import React from 'react';
import {Link} from 'react-router';

/** class nav page */
export default class NavPage extends React.Component {
  /** render function
   *  @return {JSX} */
  render() {
    return (
      <div>
        <nav className='navbar navbar-default navbar-static-top navbar-inverse'>
          <div className='container'>
            <ul className='nav navbar-nav'>
              <li className='active'>
                <Link activeClassName='active' className='btn-default btn-primary' to='/'><span className='glyphicon glyphicon-home' />Home</Link>
              </li>
              <li>
                <Link activeClassName='active' to='https://devcenter.heroku.com/articles/how-heroku-works'><span className='glyphicon glyphicon-user' />How Heroku Works</Link>
              </li>
              <li>
                <Link activeClassName='active' to='http://localhost:5000/something'><span className='glyphicon glyphicon-flash' />Test Page 3</Link>
              </li>
              <li className='dropdown'>
                <a href='#' className='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><span className='glyphicon glyphicon-info-sign' />Getting Started Guides<span className='caret' /></a>
                <ul className='dropdown-menu' role='menu'>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-ruby'>Getting Started with Ruby on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-nodejs'>Getting Started with Node on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-php'>Getting Started with PHP on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-python'>Getting Started with Python on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-java'>Getting Started with Java on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-go'>Getting Started with Go on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-clojure'>Getting Started with Clojure on Heroku</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-scala'>Getting Started with Scala on Heroku</a></li>
                  <li className='divider' />
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-heroku-and-connect-without-local-dev'>Getting Started on Heroku with Heroku Connect</a></li>
                  <li><a href='https://devcenter.heroku.com/articles/getting-started-with-jruby'>Getting Started with Ruby on Heroku (Microsoft Windows)</a></li>
                </ul>
              </li>
            </ul>
            <ul className='nav navbar-nav navbar-right'>
              <li className='navbar-right'>
                <Link activeClassName='active' to='https://devcenter.heroku.com'><span className='glyphicon glyphicon-book' />Heroku Dev Center</Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
