'use strict';

import React from 'react';

/** class index row */
export default class IndexRow extends React.Component {
  /** render function
   *  @return {JSX} */
  render() {
    return (
      <tr>
        <td>{this.props.item[0]}</td>
        <td>{this.props.item[1]}</td>
        <td>{this.props.item[2]}</td>
      </tr>
    );
  }
}

IndexRow.propTypes = {
  item: React.PropTypes.object.isRequired
};
