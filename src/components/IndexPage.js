'use strict';

import React from 'react';
import IndexRow from './IndexRow';

/** class index page */
export default class IndexPage extends React.Component {
  /** render function
   *  @return {JSX} */
  render() {
    let content = {};

    content.row1 = [1, 2, 3];
    content.row2 = [4, 5, 6];
    content.row3 = [7, 8, 9];

    let rows = [];

    for (let item in content) {
      if (content.hasOwnProperty(item)) {
        rows.push(<IndexRow item={content[item]}/>);
      }
    }

    return (
      <div>
        <div className='jumbotron text-center'>
          <div className='container'>
            <span data-test='title'>Let's put a table in here.</span>
            <table>
              {rows}
            </table>
          </div>
        </div>
      </div>
    );
  }
}
