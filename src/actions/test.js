import {
    TEST_POST,
    TEST_POST_SUCCESS,
    TEST_POST_FAILURE
} from './ActionTypes';
import axios from 'axios';

/* MEMO POST */
export function testPostRequest(contents) {
    return (dispatch) => {
        // inform TEST POST API is starting
        dispatch(testPost());

        return axios.post('/test/', { contents })
        .then((response) => {
            dispatch(testPostSuccess());
        }).catch((error) => {
            dispatch(testPostFailure(error.response.data.code));
        });
    };
}

export function testPost() {
    return {
        type: TEST_POST
    };
}

export function testPostSuccess() {
    return {
        type: TEST_POST_SUCCESS
    };
}

export function testPostFailure(error) {
    return {
        type: TEST_POST_FAILURE,
        error
    };
}