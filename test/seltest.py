from pyvirtualdisplay import Display
from selenium import webdriver

# Initialize
display = Display(visible=0, size=(800, 600))
display.start()
print 'loading chrome webdriver'
driver = webdriver.Chrome()

# Run test
print 'retrieving localhost'
driver.get('http://localhost:5000')
print driver.title

print 'testing table title'
elem = driver.find_element_by_css_selector('span[data-test=title]')
print elem.get_attribute('innerHTML')
assert "Let's put a table in here." in elem.get_attribute('innerHTML')

print 'testing database query contents'
driver.get('http://localhost:5000/db')
elem = driver.find_element_by_css_selector('.results ul li:first-child')
print elem.get_attribute('innerHTML')
assert '1 - hello database' in elem.get_attribute('innerHTML')

# Tear down
driver.quit()
display.stop()