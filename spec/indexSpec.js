const request = require('request');

const baseUrl = 'http://localhost:5000/';

describe('Hello World Server', function() {
  describe('GET /', function() {
    it('returns status code 200', function(done) {
      request.get(baseUrl, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });
  });
});
