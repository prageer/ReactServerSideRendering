/* 
   Create all tables, views, constraints, etc.
*/ 

CREATE TABLE card(
   ID SERIAL PRIMARY KEY NOT NULL,
   PROMPT CHAR(1000),
   RESPONSE CHAR(1000),
   HINT CHAR(1000)
);

CREATE TABLE choice(
    ID SERIAL PRIMARY KEY NOT NULL,
    VALUE CHAR(1000),
    CARD_ID INT references card(ID)
);

CREATE TABLE test_table(
    ID SERIAL PRIMARY KEY NOT NULL,
    value int,
    name CHAR(1000)
);
